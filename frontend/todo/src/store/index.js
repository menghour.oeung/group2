import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
  state: { add: "", token: localStorage.getItem("token") || "", cate: {} },
  mutations: {
    removeCate(state, data) {
      state.cate = state.cate.filter((item) => {
        return item.id != data;
      });
    },
    addActive(state, data) {
      state.add = data;
    },
    addToken(state, data) {
      state.token = data;
    },
    addCategory(state, data) {
      state.cate = data;
    },
    pushCategory(state, data) {
      console.log(data);
      state.cate.push({ ...data, todos: [] });
    },
    removetodo(state, data) {
      // console.log(data, state);
      let index1 = 0,
        index2 = 0;

      state.cate.forEach((item) => {
        item.todos.forEach((element) => {
          if (element.id == data) {
            state.cate[index1].todos.splice(index2, 1);
          }
          index2++;
        });
        index1++;
        index2 = 0;
      });
    },
    addcard(state, { data }) {
      const index = state.cate.findIndex(
        (element) => element.id == data.category_id
      );
      state.cate[index].todos.push({ name: data.name, id: data.id });
    },
  },
  actions: {
    async login() {
      const { data } = await axios.post(
        "http://127.0.0.1:8000/api/login",
        {
          email: "1232131",
          password: "123",
        },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      localStorage.setItem("user-token", data);
      return data;
    },
    async register({ commit }, doc) {
      const { data } = await axios.post(
        "http://127.0.0.1:8000/api/rg",
        {
          name: doc.name,
          email: doc.email,
          phone: doc.phone,
          password: doc.password,
        },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      commit("token", data);
      localStorage.setItem("user-token", data);
      return data;
    },
    async getcate({ commit }) {
      const { data } = await axios.get(
        "http://127.0.0.1:8000/api/cate?project=1",
        {
          headers: {
            Authorization: `Bearer ${this.state.token}`,
          },
        }
      );
      commit("addCategory", data);
    },
  },
  modules: {},
});
