<?php

namespace App\Http\Controllers;

use App\Models\CheckList;
use Illuminate\Http\Request;

class CheckListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'desc' => 'required',
            'todo_id' => 'required'
        ]);
        return CheckList::create(['desc' => $request->desc, 'status' => $request->status, 'todo_id' => $request->todo_id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CheckList  $checkList
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return CheckList::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CheckList  $checkList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if ($request->status) {
            $status = $request->status  === 'true' ? 1 : 0;
            return CheckList::find($id)->update(['status' => $status]);
        }
        if ($request->desc) {
            return CheckList::find($id)->update(['desc' => $request->desc]);
        }

        return null;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CheckList  $checkList
     * @return \Illuminate\Http\Response
     */
    public function destroy(CheckList $checkList, $id)
    {
        //

        CheckList::destroy($id);
    }
}
